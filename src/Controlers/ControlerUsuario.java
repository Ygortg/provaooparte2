package Controlers;

import java.util.ArrayList;

import Models.Medico;
import Models.Paciente;
import Models.Usuario;

public class ControlerUsuario {
	
	private ArrayList<Medico> listaMedico;
	private ArrayList<Paciente> listaPaciente;
	public ControlerUsuario(){
		listaMedico = new ArrayList<Medico>();
		listaPaciente = new ArrayList<Paciente>();
	}
	public void adicionarUmMedico(Medico umMedico){
		listaMedico.add(umMedico);
	}
	public void adicionarUmPaciente(Paciente umPaciente){
		listaPaciente.add(umPaciente);
	}
	public Medico buscarMedico(String umMedico){
		for (Medico buscaMedico: listaMedico){
			if(buscaMedico.getNome().equalsIgnoreCase(umMedico)){
				return buscaMedico;
			}
		}
		return null;
	}
	public Paciente buscarPaciente(String umPaciente){
		for (Paciente buscaPaciente: listaPaciente){
			if(buscaPaciente.getNome().equalsIgnoreCase(umPaciente)){
				return buscaPaciente;
			}
		}
		return null;
	}
	public void removerMedico(Medico umMedico){
		listaMedico.remove(umMedico);
	}
	public void removerPaciente(Paciente umPaciente){
		listaPaciente.remove(umPaciente);
	}
}