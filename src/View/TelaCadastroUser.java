package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JList;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

public class TelaCadastroUser extends JFrame {

	private JPanel contentPane;
	private JMenuItem mntmCadastrarMedicos;
	private TelaCadastroMedico novaTelaMedicos;
	private TelaCadastroPaciente novaTelaPaciente;
	private BuscarMedicos buscarMedicos;
	private BuscarPaciente buscarPaciente;
	private RemoverMedico removerMedico;
	private RemoverPaciente removerPaciente;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroUser frame = new TelaCadastroUser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaCadastroUser() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setExtendedState(MAXIMIZED_BOTH);  
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Menu");
		menuBar.add(mnNewMenu);
		
		JList list = new JList();
		mnNewMenu.add(list);
		
		JMenu mnCadasro = new JMenu("Cadasro");
		mnNewMenu.add(mnCadasro);
		
		mntmCadastrarMedicos = new JMenuItem("Cadastrar medicos");
		
		
		mnCadasro.add(mntmCadastrarMedicos);
		
		JMenuItem mntmCadastrarPacientes = new JMenuItem("Cadastrar pacientes");
		
		mnCadasro.add(mntmCadastrarPacientes);
		
		JMenuItem mntmCadastrarConsulta = new JMenuItem("Cadastrar Consulta");
		mnCadasro.add(mntmCadastrarConsulta);
		
		JMenu mnBuscar = new JMenu("Buscar");
		mnNewMenu.add(mnBuscar);
		
		JMenuItem menuBuscaMedico = new JMenuItem("Medico");
		
		mnBuscar.add(menuBuscaMedico);
		
		JMenuItem menuBuscaPaciente = new JMenuItem("Paciente");
		
		mnBuscar.add(menuBuscaPaciente);
		
		JMenuItem mntmBuscaConsulta = new JMenuItem("Consulta");
		mnBuscar.add(mntmBuscaConsulta);
		
		JMenu mnRemover = new JMenu("Remover");
		mnNewMenu.add(mnRemover);
		
		JMenuItem mntmRemoverMedico = new JMenuItem("Medico");
		
		mnRemover.add(mntmRemoverMedico);
		
		JMenuItem mntmRemoverPaciente = new JMenuItem("Paciente");
		
		mnRemover.add(mntmRemoverPaciente);
		
		JMenuItem mntmRemoverConsulta = new JMenuItem("Consulta");
		mnRemover.add(mntmRemoverConsulta);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(192, 192, 192));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBackground(Color.LIGHT_GRAY);
		desktopPane.setBounds(40, 20, 1000, 800);
		contentPane.add(desktopPane);
		mntmCadastrarMedicos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(novaTelaMedicos==null || novaTelaMedicos.isClosed()){
				novaTelaMedicos = new TelaCadastroMedico();
				desktopPane.add(novaTelaMedicos);
				novaTelaMedicos.show();
				}
			}
		});
		mntmCadastrarPacientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(novaTelaPaciente==null || novaTelaPaciente.isClosed()){
					novaTelaPaciente = new TelaCadastroPaciente();
					desktopPane.add(novaTelaPaciente);
					novaTelaPaciente.show();
					}
				
			}
		});
		menuBuscaMedico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(buscarMedicos==null || buscarMedicos.isClosed()){
					buscarMedicos = new BuscarMedicos();
					desktopPane.add(buscarMedicos);
					buscarMedicos.show();
					}
			}
		});	
		menuBuscaPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(buscarPaciente==null || buscarPaciente.isClosed()){
					buscarPaciente = new BuscarPaciente();
					desktopPane.add(buscarPaciente);
					buscarPaciente.show();
					}
			}
		});
		mntmRemoverMedico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(removerMedico==null || removerMedico.isClosed()){
					removerMedico = new RemoverMedico();
					desktopPane.add(removerMedico);
					removerMedico.show();
					}	
			}
		});
		mntmRemoverPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(removerPaciente==null || removerPaciente.isClosed()){
					removerPaciente = new RemoverPaciente();
					desktopPane.add(removerPaciente);
					removerPaciente.show();
				}	
			}
		});
	}
}
