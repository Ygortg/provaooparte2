package View;

import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;

import Controlers.ControlerUsuario;
import Models.Paciente;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaCadastroPaciente extends JInternalFrame {

	private JTextField txtNome;
	private JTextField txtIdade;
	private JTextField txtTelefone;
	private JTextField txtRg;
	private JTextField txtCpf;
	private JTextField txtSintomas;
	
	ButtonGroup grupoAtendimento = new ButtonGroup();
	static ControlerUsuario umControladorUsuario;
	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public TelaCadastroPaciente() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		

		umControladorUsuario = new ControlerUsuario();
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(12, 12, 70, 15);
		getContentPane().add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setText("Nome");
		txtNome.setBounds(100, 10, 114, 19);
		getContentPane().add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblIdade = new JLabel("Idade");
		lblIdade.setBounds(12, 37, 70, 15);
		getContentPane().add(lblIdade);
		
		txtIdade = new JTextField();
		txtIdade.setText("Idade");
		txtIdade.setBounds(100, 35, 114, 19);
		getContentPane().add(txtIdade);
		txtIdade.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(12, 60, 70, 15);
		getContentPane().add(lblTelefone);
		
		txtTelefone = new JTextField();
		txtTelefone.setText("Telefone");
		txtTelefone.setBounds(100, 58, 114, 19);
		getContentPane().add(txtTelefone);
		txtTelefone.setColumns(10);
		
		JLabel lblRg = new JLabel("RG");
		lblRg.setBounds(12, 85, 70, 15);
		getContentPane().add(lblRg);
		
		txtRg = new JTextField();
		txtRg.setText("RG");
		txtRg.setBounds(100, 83, 114, 19);
		getContentPane().add(txtRg);
		txtRg.setColumns(10);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setBounds(12, 112, 70, 15);
		getContentPane().add(lblCpf);
		
		txtCpf = new JTextField();
		txtCpf.setText("CPF");
		txtCpf.setBounds(100, 110, 114, 19);
		getContentPane().add(txtCpf);
		txtCpf.setColumns(10);
		
		JLabel lblSintomas = new JLabel("Sintomas");
		lblSintomas.setBounds(12, 139, 70, 15);
		getContentPane().add(lblSintomas);
		
		txtSintomas = new JTextField();
		txtSintomas.setText("Sintomas");
		txtSintomas.setBounds(100, 137, 114, 19);
		getContentPane().add(txtSintomas);
		txtSintomas.setColumns(10);
		
		final JRadioButton rdbtnPediatria = new JRadioButton("Pediatria");
		rdbtnPediatria.setBounds(12, 199, 149, 23);
		getContentPane().add(rdbtnPediatria);
		
		JRadioButton rdbtnClinicaMdica = new JRadioButton("Clinica médica");
		rdbtnClinicaMdica.setBounds(180, 199, 149, 23);
		getContentPane().add(rdbtnClinicaMdica);
		
		grupoAtendimento.add(rdbtnPediatria);
		grupoAtendimento.add(rdbtnClinicaMdica);
		
		JButton btnSubmeter = new JButton("Submeter");
		btnSubmeter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nome,idade,telefone,rg,cpf,sintomas;
				boolean pediatra, clinicaMedica;
				Paciente umPaciente;
				nome = txtNome.getText();
				telefone = txtTelefone.getText();
				idade = txtIdade.getText();
				rg = txtRg.getText();
				cpf = txtCpf.getText();
				sintomas = txtSintomas.getText();
				if(rdbtnPediatria.isSelected()){
					pediatra = true;
					clinicaMedica=false;
				}
				else {
					pediatra = false;
					clinicaMedica = true;
				}
				umPaciente = new Paciente(nome, sintomas);
				umPaciente.setIdade(idade);
				umPaciente.setCpf(cpf);
				umPaciente.setRg(rg);
				umPaciente.setClinicaMedica(clinicaMedica);
				umPaciente.setPediatria(pediatra);
				umPaciente.setTelefone(telefone);
				umControladorUsuario.adicionarUmPaciente(umPaciente);
				JOptionPane.showMessageDialog(null, "Paciente cadastrado");
				dispose();
			}
		});
		btnSubmeter.setBounds(286, 7, 117, 25);
		getContentPane().add(btnSubmeter);
		
		JButton btnFexar = new JButton("Fexar");
		btnFexar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnFexar.setBounds(286, 55, 117, 25);
		getContentPane().add(btnFexar);

	}
}
