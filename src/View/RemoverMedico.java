package View;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import Models.Medico;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RemoverMedico extends JInternalFrame {
	private JTextField txtRemover;
	private Medico umMedico;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RemoverMedico frame = new RemoverMedico();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RemoverMedico() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
	
		
		JLabel lblRemoverUmMedico = new JLabel("Remover um medico");
		lblRemoverUmMedico.setBounds(12, 12, 164, 15);
		getContentPane().add(lblRemoverUmMedico);
		
		txtRemover = new JTextField();
		txtRemover.setText("remover");
		txtRemover.setBounds(219, 10, 114, 19);
		getContentPane().add(txtRemover);
		txtRemover.setColumns(10);
		
		JButton btnRemover = new JButton("Remover");
		btnRemover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nomeRemover;
				
				nomeRemover = txtRemover.getText();
				
				try{
					umMedico = TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeRemover);
					JOptionPane.showMessageDialog(null,"Nome: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeRemover).getNome()+
							"\nCRM: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeRemover).getRegistroMedico()+
							"\nIdade: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeRemover).getIdade()+
							"\nCPF: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeRemover).getCpf()+
							"\nRG: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeRemover).getRg()+
							"\nArea de atuação: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeRemover).getAreaAtuacao()+
							"\nTempo de serviço: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeRemover).getTempoDeServico()+
							"\nTelefone: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeRemover).getTelefone()+
							"\n\n\n\n\n Foi removido com sucesso");
					TelaCadastroMedico.umControladorUsuario.removerMedico(umMedico);
					
				}catch(NullPointerException b){
					JOptionPane.showMessageDialog(null, "Medico não cadastrado");
				}
			}
		});
		btnRemover.setBounds(59, 206, 117, 25);
		getContentPane().add(btnRemover);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnFechar.setBounds(261, 206, 117, 25);
		getContentPane().add(btnFechar);

	}

}
