package View;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import Models.Paciente;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RemoverPaciente extends JInternalFrame {
	private JTextField txtRemover;
	private Paciente umPaciente;

	/**
	 * Create the frame.
	 */
	public RemoverPaciente() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblRemoverUmPaciente = new JLabel("Remover um paciente");
		lblRemoverUmPaciente.setBounds(12, 12, 168, 15);
		getContentPane().add(lblRemoverUmPaciente);
		
		txtRemover = new JTextField();
		txtRemover.setText("remover");
		txtRemover.setBounds(198, 10, 114, 19);
		getContentPane().add(txtRemover);
		txtRemover.setColumns(10);
		
		JButton btnRemover = new JButton("Remover");
		btnRemover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
String nomeRemover;
				
				nomeRemover = txtRemover.getText();
				
				try{
					umPaciente = TelaCadastroPaciente.umControladorUsuario.buscarPaciente(nomeRemover);
					JOptionPane.showMessageDialog(null,"Nome: "+TelaCadastroPaciente.umControladorUsuario.buscarPaciente(nomeRemover).getNome()+
							"\nIdade: "+TelaCadastroPaciente.umControladorUsuario.buscarPaciente(nomeRemover).getIdade()+
							"\nCPF: "+TelaCadastroPaciente.umControladorUsuario.buscarPaciente(nomeRemover).getCpf()+
							"\nRG: "+TelaCadastroPaciente.umControladorUsuario.buscarPaciente(nomeRemover).getRg()+
							"\nSintomas: "+TelaCadastroPaciente.umControladorUsuario.buscarPaciente(nomeRemover).getSintomas()+
							"\nTelefone: "+TelaCadastroPaciente.umControladorUsuario.buscarPaciente(nomeRemover).getTelefone()+
							"\n\n\n\n\n Foi removido com sucesso");
					TelaCadastroPaciente.umControladorUsuario.removerPaciente(umPaciente);
					
				}catch(NullPointerException b){
					JOptionPane.showMessageDialog(null, "Medico não cadastrado");
				}
			}
		});
		btnRemover.setBounds(74, 213, 117, 25);
		getContentPane().add(btnRemover);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnFechar.setBounds(269, 213, 117, 25);
		getContentPane().add(btnFechar);

	}

}
