

package Models;

public class Medico extends Usuario{
	
	private String registroMedico, areaAtuacao, tempoDeServico;
	private boolean confirmaCirurgiao;
	
	
	
	public Medico(String registroMedico,String nome) {
		super(nome);
		this.registroMedico = registroMedico;
	}
	public String getRegistroMedico() {
		return registroMedico;
	}
	public void setRegistroMedico(String registroMedico) {
		this.registroMedico = registroMedico;
	}
	public String getAreaAtuacao() {
		return areaAtuacao;
	}
	public void setAreaAtuacao(String areaAtuacao) {
		this.areaAtuacao = areaAtuacao;
	}
	public String getTempoDeServico() {
		return tempoDeServico;
	}
	public void setTempoDeServico(String tempoDeServico) {
		this.tempoDeServico = tempoDeServico;
	}
	public boolean isConfirmaCirurgiao() {
		return confirmaCirurgiao;
	}
	public void setConfirmaCirurgiao(boolean confirmaCirurgiao) {
		this.confirmaCirurgiao = confirmaCirurgiao;
	}
}

