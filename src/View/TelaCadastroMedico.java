package View;

import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JProgressBar;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

import javax.swing.DropMode;

import Controlers.ControlerUsuario;
import Models.Medico;

public class TelaCadastroMedico extends JInternalFrame {
	private JTextField txtNome;
	private JTextField txtIdade;
	private JTextField txtTelefone;
	private JTextField txtRg;
	private JTextField txtCpf;
	private JTextField txtCrm;
	private JTextField txtAreaDeAtuao;
	private JTextField txtTempoDeServio;
	ButtonGroup grupoCirurgiao = new ButtonGroup();
	
	static ControlerUsuario umControladorUsuario;

	/**
	 * Launch the application.

	/**
	 * Create the frame.
	 */
	public TelaCadastroMedico() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		umControladorUsuario = new ControlerUsuario();
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(12, 12, 70, 15);
		getContentPane().add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setText("Nome");
		txtNome.setBounds(100, 10, 114, 19);
		getContentPane().add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblIdade = new JLabel("Idade");
		lblIdade.setBounds(12, 37, 70, 15);
		getContentPane().add(lblIdade);
		
		txtIdade = new JTextField();
		txtIdade.setText("Idade");
		txtIdade.setBounds(100, 35, 114, 19);
		getContentPane().add(txtIdade);
		txtIdade.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(12, 60, 70, 15);
		getContentPane().add(lblTelefone);
		
		txtTelefone = new JTextField();
		txtTelefone.setText("Telefone");
		txtTelefone.setBounds(100, 58, 114, 19);
		getContentPane().add(txtTelefone);
		txtTelefone.setColumns(10);
		
		JLabel lblRg = new JLabel("RG");
		lblRg.setBounds(12, 85, 70, 15);
		getContentPane().add(lblRg);
		
		txtRg = new JTextField();
		txtRg.setText("RG");
		txtRg.setBounds(100, 83, 114, 19);
		getContentPane().add(txtRg);
		txtRg.setColumns(10);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setBounds(12, 112, 70, 15);
		getContentPane().add(lblCpf);
		
		txtCpf = new JTextField();
		txtCpf.setText("CPF");
		txtCpf.setBounds(100, 110, 114, 19);
		getContentPane().add(txtCpf);
		txtCpf.setColumns(10);
		
		JLabel lblCrm = new JLabel("CRM");
		lblCrm.setBounds(12, 137, 70, 15);
		getContentPane().add(lblCrm);
		
		txtCrm = new JTextField();
		txtCrm.setText("CRM");
		txtCrm.setBounds(100, 135, 114, 19);
		getContentPane().add(txtCrm);
		txtCrm.setColumns(10);
		
		JLabel lblAreaDeAtuao = new JLabel("Area de Atuação");
		lblAreaDeAtuao.setBounds(12, 164, 132, 15);
		getContentPane().add(lblAreaDeAtuao);
		
		txtAreaDeAtuao = new JTextField();
		txtAreaDeAtuao.setText("Area de atuação");
		txtAreaDeAtuao.setBounds(144, 162, 114, 19);
		getContentPane().add(txtAreaDeAtuao);
		txtAreaDeAtuao.setColumns(10);
		
		JLabel lblTempoDeServio = new JLabel("Tempo de Serviço");
		lblTempoDeServio.setBounds(12, 191, 124, 15);
		getContentPane().add(lblTempoDeServio);
		
		txtTempoDeServio = new JTextField();
		txtTempoDeServio.setText("Tempo de Serviço");
		txtTempoDeServio.setBounds(144, 189, 114, 19);
		getContentPane().add(txtTempoDeServio);
		txtTempoDeServio.setColumns(10);
		
		JLabel lblCirurgio = new JLabel("Cirurgião");
		lblCirurgio.setBounds(12, 224, 70, 15);
		getContentPane().add(lblCirurgio);
		
		final JRadioButton rdbtnSim = new JRadioButton("Sim");
		rdbtnSim.setToolTipText("confirma");
		rdbtnSim.setBounds(100, 220, 70, 23);
		getContentPane().add(rdbtnSim);
		
		JRadioButton rdbtnNo = new JRadioButton("Não");
		rdbtnNo.setToolTipText("confirma");
		rdbtnNo.setBounds(192, 220, 84, 23);
		getContentPane().add(rdbtnNo);
		
		grupoCirurgiao.add(rdbtnSim);
		grupoCirurgiao.add(rdbtnNo);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBackground(Color.WHITE);
		progressBar.setStringPainted(true);
		progressBar.setBounds(150, 251, 148, 14);
		getContentPane().add(progressBar);
		
		JButton btnSubmeter = new JButton("Submeter");
		btnSubmeter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nome,idade,telefone,rg,cpf,crm,areaAtuacao,tempoDeServico;
				boolean cirurgiao;
				Medico umMedico;
				nome = txtNome.getText();
				telefone = txtTelefone.getText();
				idade = txtIdade.getText();
				rg = txtRg.getText();
				cpf = txtCpf.getText();
				crm = txtCrm.getText();
				areaAtuacao = txtAreaDeAtuao.getText();
				tempoDeServico = txtTempoDeServio.getText();
				if(rdbtnSim.isSelected()){
				cirurgiao = true;
				}
				else {
					cirurgiao = false;
				}
			
				
				umMedico = new Medico(crm, nome);
				umMedico.setIdade(idade);
				umMedico.setAreaAtuacao(areaAtuacao);
				umMedico.setCpf(cpf);
				umMedico.setConfirmaCirurgiao(cirurgiao);
				umMedico.setRg(rg);
				umMedico.setTelefone(telefone);
				umMedico.setTempoDeServico(tempoDeServico);
				umControladorUsuario.adicionarUmMedico(umMedico);
				JOptionPane.showMessageDialog(null, "Medico cadastrado");
								
			}
		});
		btnSubmeter.setBounds(286, 7, 117, 25);
		getContentPane().add(btnSubmeter);
		
		JButton btnFexar = new JButton("Fexar");
		btnFexar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnFexar.setBounds(286, 55, 117, 25);
		getContentPane().add(btnFexar);

	}
}
