package View;

import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;

import java.awt.Choice;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextArea;
import javax.swing.JTextField;

import Controlers.ControlerUsuario;
import Models.Medico;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BuscarMedicos extends JInternalFrame {
	private JTextField txtNome;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuscarMedicos frame = new BuscarMedicos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BuscarMedicos() {
		getContentPane().setFont(new Font("WenQuanYi Micro Hei", Font.PLAIN, 12));
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblBusqueUmNome = new JLabel("Busque um Nome");
		lblBusqueUmNome.setFont(new Font("Dialog", Font.BOLD, 12));
		lblBusqueUmNome.setBounds(12, 28, 169, 15);
		getContentPane().add(lblBusqueUmNome);
		
		JTextArea txtrNome = new JTextArea();
		txtrNome.setText("nome");
		txtrNome.setBounds(182, 28, 1, 15);
		getContentPane().add(txtrNome);
		
		txtNome = new JTextField();
		txtNome.setText("Nome");
		txtNome.setBounds(172, 26, 114, 19);
		getContentPane().add(txtNome);
		txtNome.setColumns(10);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nomeBusca;
				
				nomeBusca = txtNome.getText();
				
				try{
					JOptionPane.showMessageDialog(null,"Nome: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeBusca).getNome()+
							"\nCRM: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeBusca).getRegistroMedico()+
							"\nIdade: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeBusca).getIdade()+
							"\nCPF: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeBusca).getCpf()+
							"\nRG: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeBusca).getRg()+
							"\nArea de atuação: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeBusca).getAreaAtuacao()+
							"\nTempo de serviço: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeBusca).getTempoDeServico()+
							"\nTelefone: "+TelaCadastroMedico.umControladorUsuario.buscarMedico(nomeBusca).getTelefone());
					
					
				}catch(NullPointerException b){
					JOptionPane.showMessageDialog(null, "Medico não cadastrado");
				}
			}
		});
		btnBuscar.setActionCommand("Buscar");
		btnBuscar.setBounds(64, 217, 117, 25);
		getContentPane().add(btnBuscar);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnFechar.setBounds(240, 217, 117, 25);
		getContentPane().add(btnFechar);

	}
}
